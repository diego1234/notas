<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class notes extends Model
{
    protected $table = 'notes';

    protected $primarykey = 'id_notes';
    public $timestamps = true;
    const CREATED_AT = 'created_ad';
    const UPDATED_AT = 'updated_ad';



    protected $fillabel = [
      'notes_additional',
      'observation',
      'created_at',
      'updated_at'
    ];
}
