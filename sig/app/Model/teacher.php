<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class teacher extends Model
{
    protected $table = 'teacher';

    protected $primarykey = 'id_teacher';
    public $timestamps = true;
    const CREATED_AT = 'created_ad';
    const UPDATED_AT = 'updated_ad';



    protected $fillabel = [
      'name_teacher',
      'surname_teacher',
      'specialty',
      'telephone'
    ];
}
