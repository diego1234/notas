<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class matter extends Model
{
    protected $table = 'matter';

    protected $primarykey = 'id_matter';
    public $timestamps = true;
    const CREATED_AT = 'created_ad';
    const UPDATED_AT = 'updated_ad';



    protected $fillabel = [
      'matter',
      'level_education',
      'schedule'
    ];
}
