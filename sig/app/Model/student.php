<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class student extends Model
{
    protected $table = 'student';
    protected $primarykey = 'id';
    public $timestamps = true;
    const CREATED_AT = 'created_ad';
    const UPDATED_AT = 'updated_ad';

    protected $fillabel = [
      'name',
      'surname',
      'birthdate',
      'age',
      'home'
    ];
}